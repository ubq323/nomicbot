import discord 
from discord.ext import commands 

import sys, os
import datetime
import math

import random
import asyncio

import logging
logging.basicConfig(level=logging.INFO, format="[%(asctime)s] %(levelname)s [%(name)s] %(message)s")
logger = logging.getLogger("nomicbot")

from nomicbot import config

bot = commands.Bot("-")

@bot.command()
async def ping(ctx):
    await ctx.send("pong")

@commands.has_role(config.roles["player"])
@bot.command()
async def inactivity(ctx, num_hours:int=72):
    if num_hours <= 0:
        return await ctx.send("num_hours must be non-negative")
    if num_hours > 98: # one week
        return await ctx.send("num hours can't be above 98")

    
    # current text of rule 018:
    # > If a player has been part of the game for at least 72 hours, and that player has
    # > not sent any messages in the conic nomic Discord server within the last 72 hours,
    # > then that player is removed from the game (as defined by rule 13)
    # we can't tell if they've been part of the game for at least 72 hours easily
    # so we just return players that match the latter part
    # ie players that haven't sent any messages in any channel in the last n hours

    async with ctx.typing():
        now = datetime.datetime.utcnow()
        time_threshold = now - datetime.timedelta(hours=num_hours)
        await ctx.send(f"now = {now}, time_threshold = {time_threshold}")

        players = bot.get_guild(config.guild_id).get_role(config.roles["player"]).members
        inactives = set(p.id for p in players)
        for ch in bot.get_guild(config.guild_id).text_channels:
            if ch.id in config.inactivity_ignore_channels:
                continue
            logger.info(ch.name)
            async for msg in ch.history(after=time_threshold, limit=None):
                if msg.author.id in inactives:
                    inactives.remove(msg.author.id)
        
        if len(inactives) == 0:
            await ctx.send(f"I couldn't find any players who haven't sent any messages in the last {num_hours} hours.")
        else:
            out = f"As far as I could tell, the following users have not sent any messages in the last {num_hours} hours:\n"
            for uid in inactives:
                u = bot.get_user(uid)
                if "THEPOTATOISHOT" in u.name.upper().replace(" ",""):
                    n = "*(I am not going to say this username)*"
                else:
                    n = u.name
                out += f"{n}#{u.discriminator}\n"
            await ctx.send(out)
            
@commands.has_role(config.roles["player"])
@bot.command()
async def players(ctx):
    players = bot.get_guild(config.guild_id).get_role(config.roles["player"]).members
    count = len(players)
    vth = math.floor(count/2)
    await ctx.send(f"I count {count} accounts with the player role. floor({count}/2) = {vth}.")

potatoing = False
@commands.is_owner()
@bot.command()
async def potato_once(ctx):
    global potatoing
    if potatoing:
        return await ctx.send("already potatoing")
    potatoing = True
    await ctx.send("The potato is hot")
    while True:
        await asyncio.sleep(60 * (random.randint(0,2000)))
        await ctx.send("The potato is hot")

@bot.command()
async def say(ctx, *, what: str):
    await ctx.send(what)

    
def main():
    token = os.getenv("TOKEN",None)
    if token is None:
        sys.exit("please set envvar TOKEN to your bot token")
    bot.run(token.strip())

main()
